package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ljam763 on 8/05/2017.
 */
public class NestingShape extends Shape{


    public List<Shape> getChildren() {
        return children;
    }

    private List<Shape> children = new ArrayList<>();
    /**
     * Creates a ictgradschool.industry.lab15.ex01.Shape object with default values for instance variables.
     */
    public NestingShape() {
        this(DEFAULT_X_POS, DEFAULT_Y_POS, DEFAULT_DELTA_X, DEFAULT_DELTA_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    /**
     * Creates a ictgradschool.industry.lab15.ex01.Shape object with a specified x and y position.
     */
    public NestingShape(int x, int y) {
        this(x, y, DEFAULT_DELTA_X, DEFAULT_DELTA_Y, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    /**
     * Creates a ictgradschool.industry.lab15.ex01.Shape instance with specified x, y, deltaX and deltaY values.
     * The ictgradschool.industry.lab15.ex01.Shape object is created with a default width and height.
     */
    public NestingShape(int x, int y, int deltaX, int deltaY) {
        this(x, y, deltaX, deltaY, DEFAULT_WIDTH, DEFAULT_HEIGHT);
    }

    /**
     * Creates a ictgradschool.industry.lab15.ex01.Shape instance with specified x, y, deltaX, deltaY, width and
     * height values.
     */
    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height) {
     super(x, y, deltaX, deltaY, width, height);
    }

    public void move(int width, int height){
        super.move(width,height);
        for (Shape child : children) {
            child.move(this.fWidth,this.fHeight);
        }

    }

    @Override
    public void paint(Painter painter) {
        painter.drawRect(fX,fY,fHeight,fWidth);
        painter.translate(fX ,fY);
        for (Shape child : children) {
            child.paint(painter);

        }
        painter.translate(-fX ,-fY);
    }

    public void add(Shape child) throws IllegalArgumentException{
        if (child.getWidth() +child.getX() > this.getWidth() || child.getHeight() +child.getY() > this.getHeight()){
            System.out.println(this.getWidth());
            System.out.println(this.getHeight());
            System.out.println(child.fWidth);
            System.out.println(child.fHeight);
            throw new IllegalArgumentException("Out of bounds");
        }
        if (child.parent == null){
            children.add(child);
            child.setParent(this);
        }
        else if (child.parent != null){
            throw new IllegalArgumentException("Already have a parent.");
        }

//        if (child.fX + this.fX + child.fWidth > this.fX + this.fWidth ||child.fY + this.fY + child.fHeight > this.fY + this.fHeight || child.fX + this.fX < this.fX || child.fY + this.fY < this.fY){
//            System.out.println(child.getX()+child.getWidth());
//            throw new IllegalArgumentException("Out of bounds");
//        }
//        if (child.fX  > this.fX + this.fWidth -child.fWidth || child.fY > this.fY + this.fHeight-child.fHeight || child.fY < this.fY || child.fX < this.fX){
//
//        }
    }
    public void remove(Shape child){
        if (children.contains(child)){
            children.remove(child);
            child.setParent(null);
        }
    }
    public Shape shapeAt(int index) throws IndexOutOfBoundsException{
        if (index< 0 || index > children.size()-1){
            throw new IndexOutOfBoundsException("Index out of Bounds");
        }
        return children.get(index);
    }
    public int shapeCount(){
        return children.size();
    }


    public int indexOf(Shape child){
        for (int i = 0; i < shapeCount(); i++) {
            if (children.get(i).equals(child)){
                return i;
            }
        }
        return -1;
    }
    public boolean contains(Shape child){
        if (children.contains(child)){
            return true;
        }
        else{
            return false;
        }
    }
}
