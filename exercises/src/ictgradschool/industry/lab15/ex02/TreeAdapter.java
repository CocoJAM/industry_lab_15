package ictgradschool.industry.lab15.ex02;

import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by ljam763 on 8/05/2017.
 */
public class TreeAdapter implements TreeModel {

    private NestingShape _adaptee;

    private List<TreeModelListener> _treeModelListeners;

    public TreeAdapter(NestingShape _adaptee) {
        this._adaptee = _adaptee;
        _treeModelListeners = new ArrayList<>();
    }

    @Override
    public Object getRoot() {
        return _adaptee;
    }

    @Override
    public Object getChild(Object parent, int index) {
        Object result = 0;
        Shape file = (Shape) parent;

        if (file instanceof NestingShape) {
            NestingShape directory = (NestingShape) file;
            result = directory.shapeAt(index);
        }
        return result;
    }

    @Override
    public int getChildCount(Object parent) {
        int result = 0;
        Shape file = (Shape) parent;

        if (file instanceof NestingShape) {
            NestingShape directory = (NestingShape) file;
            result = directory.shapeCount();
        }
        return result;
    }

    @Override
    public boolean isLeaf(Object node) {
        return !(node instanceof NestingShape);
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    @Override
    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;

        if (parent instanceof NestingShape) {
            NestingShape dir = (NestingShape) parent;
            indexOfChild = dir.indexOf((Shape) child);
        }
        return indexOfChild;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {
        _treeModelListeners.add(l);
    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {
        _treeModelListeners.add(l);
    }
}
